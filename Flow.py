class Flow:
    """An instance of flow, identified by src, dst, and rate, for simulation"""

    # may need to implement a hash table
    def __init__(self, src, dst, rate):
        self.src = src
        self.dst = dst
        self.rate = rate

