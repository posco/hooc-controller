class Policy:
    """ An instance of police """

    def __init__(self):
        self.chain = [ ] # expecting a list of NFs
        self.reordered_chains = []
        self.len = 0

    def add_nf(self, nf):
        self.chain.append(nf)
        self.update_length()

    def update_length(self):
        self.len = len(self.chain)

    #swapping dropper and static
    def reorder(self):
        print "doing reordering: ", [nf.type for nf in self.chain]
        for i in range(0, self.len):
            if i < self.len-1: #last item, do nothing
                if self.chain[i].nf_class is 'NF_CLASS_DROPER' and self.chain[i+1].nf_class is 'NF_CLASS_STATIC':
                    reordered_chain = list(self.chain)
                    reordered_chain[i] = self.chain[i+1]
                    reordered_chain[i+1] = self.chain[i]
                    self.reordered_chains.append(reordered_chain)
                    print "reodered\n"
            else:
                print "reodering not possible, give up"

