import random
from enum import Enum

class NetworkFunction:
    """a single instance of nf, n_i"""

    def __init__(self):
        self.type = ''
        self.cap = 0  # in pps, packets per second
        self.location = 0
        self.state = []  # empty list, no predefined states
        self.default_typeset_list = ['FW', 'IPS', 'RE', 'LB', 'IDS', 'Monitor']
        self.default_class = {'FW': 'NF_CLASS_DROPER', 'IPS':'NF_CLASS_DROPER',\
                              'RE':'NF_CLASS_MODIFIER', 'LB':'NF_CLASS_MODIFIER',\
                              'IDS':'NF_CLASS_STATIC', 'Monitor':'NF_CLASS_STATIC'}
        self.nf_class = ''
        self.set_type()

    def set_type(self):
        self.type = self.default_typeset_list[random.randint(0, 5)]
        self.cap = random.randint(81274, 1488096)  # in pps, packets per second
        self.nf_class = self.default_class[self.type]

    def place_onto_nfb(self,nf_location):
        self.location = nf_location
