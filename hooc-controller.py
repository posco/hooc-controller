# Copyright (C) 2016 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib import dpid as dpid_lib
from ryu.lib import stplib
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import lldp
from ryu.lib.packet import icmp
from ryu.app import simple_switch_13
from ryu.topology import event, switches
from ryu.topology.api import get_switch, get_link

import networkx as nx

import threading
import time
import random
import os

from NetworkFunctionBox import NetworkFunctionBox
from NetworkFunction import NetworkFunction
from Policy import Policy

#class HoocController(simple_switch_13.SimpleSwitch13):
class HoocController(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
    #_CONTEXTS = {'stplib': stplib.Stp}

    def __init__(self, *args, **kwargs):
        super(HoocController, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        #self.stp = kwargs['stplib']

        # Sample of stplib config.
        #  please refer to stplib.Stp.set_config() for details.
        #config = {dpid_lib.str_to_dpid('0000000000000001'):
        #          {'bridge': {'priority': 0x8000}},
        #          dpid_lib.str_to_dpid('0000000000000002'):
        #          {'bridge': {'priority': 0x9000}},
        #          dpid_lib.str_to_dpid('0000000000000003'):
        #          {'bridge': {'priority': 0xa000}}}
        #self.stp.set_config(config)

        self.topology_api_app = self
        self.net = nx.DiGraph()
        self.nodes = {}
        self.links = {}
        self.no_of_nodes = 0
        self.no_of_links = 0
        self.i = 0
        self.j = 0
        self.k = 18
        self.count = 0
        self.last_check = 0;
        self.D = {}
        self.previous_time_stamp = time.time()
        self.all_nfbs = []
        self.all_hosts = []
        self.is_topology_discovered = False
        self.is_greedy = False

        self.initThread = threading.Thread(target=self.initialise_setup)
        self.initThread.start()

        self.workThread = threading.Thread(target=self.construct_full_topology)
        self.workThread.start()

    def initialise_setup(self):

        self.policies = []

        # we need 100 policies to play with
        for i in range (0,10):
            p = Policy()
            policy_len = random.randrange(1, 5)
            for j in range(0, policy_len):
                nf = NetworkFunction()
                p.add_nf(nf)
            p.reorder()
            self.policies.append(p)


    def construct_full_topology(self):

        # Assume that the topology can be learned in 5 seconds and then we can manupulate it.
        #print nx.average_shortest_path_length(self.net)
        #sorted_in_degree = sorted(self.net.in_degree().values())
        #print sorted(self.net.in_degree().values())[0]

        lasped_time = time.time() - self.previous_time_stamp
        while lasped_time < 5 or self.net.number_of_nodes() <= 0:
            print lasped_time
            print self.net.number_of_nodes()
            time.sleep(3)
            lasped_time = time.time() - self.previous_time_stamp


        print "Finding TOR nodes"

        print lasped_time
        print self.net.number_of_nodes()


        edge_nodes = [n for n, d in self.net.in_degree().items() if d == sorted(self.net.in_degree().values())[0]]

        '''
        print edge_nodes
        self.net.node[edge_nodes[0]]['type']='host'
        print edge_nodes[0]
        print self.net.node[edge_nodes[0]]
        print self.net.predecessors(edge_nodes[0])
        print self.net.predecessors(self.net.predecessors(edge_nodes[0])[0])
        print "Found TOR nodes"
        '''

        self.label_nodes(edge_nodes)

        #nfb = NetworkFunctionBox(10,'fw')

        #self.net.node[edge_nodes[0]]['nfb']= nfb

        #print "adding hosts to TOR nodes"
        self.add_hosts(edge_nodes)
        #print "added"

        #print self.net.edges()
        #for link in links_list:
        #print link.dst
            #print link.src
            #print "Novo link"
        #self.no_of_links += 1

        #for n in self.net.nodes():
        #    print (n, '   ', self.net.node[n])

        #for nfb in self.all_nfbs:
        #    print (nfb,'   ', self.net.node[nfb]['nfb'].typeset)

        for nfb_id_i in (self.all_nfbs + self.all_hosts):
            self.D[nfb_id_i] = {}
            for nfb_id_j in self.all_nfbs:
                if nfb_id_i == nfb_id_j:
                    self.D[nfb_id_i][nfb_id_j] = float('inf')
                else:
                    self.D[nfb_id_i][nfb_id_j]=random.uniform(0.00001,0.01)
                    #[random.random.uniform(0.00001,0.01) for nfb_id_j in self.all_nfbs]

        self.is_topology_discovered = True

        #os._exit(0)

    def add_nfbs(self, tor_nodes, agg_nodes, core_nodes):
        #how many MBs
        #how many SWs
        #how many NFV servers

        for n in tor_nodes + agg_nodes + core_nodes:
            self.net.node[n]['nfb'] = NetworkFunctionBox('sw')
            self.all_nfbs.append(n)

        i = 10000

        for n in random.sample(tor_nodes, self.k / 4) + \
                 random.sample(agg_nodes, self.k / 4) + \
                 random.sample(core_nodes, self.k / 4):
            self.net.add_edge(n, i+n)
            self.net.add_edge(i+n, n)
            self.net.node[i+n]['nfb'] = NetworkFunctionBox('hw')
            self.net.node[i+n]['type'] = 'mb'
            self.all_nfbs.append(i+n)


    def label_nodes(self, tor_nodes):
        agg_nodes=[]
        core_nodes=[]
        for tor in tor_nodes:
            print ('tor', '\t', tor)
            self.net.node[tor]['type'] = 'tor' #label TORs
            for agg in self.net.predecessors(tor):
                if agg not in agg_nodes:
                    print ('agg', '\t',agg)
                    agg_nodes.append(agg)
                    self.net.node[agg]['type']='agg'
                    for core in self.net.predecessors(agg):
                        if core not in (tor_nodes + core_nodes):
                            print ('core', '\t', core)
                            core_nodes.append(core)
                            self.net.node[core]['type'] = 'core'

        self.add_nfbs(tor_nodes,agg_nodes,core_nodes)

    def add_hosts(self, Tor_SWs):
        i=0
        for sw in Tor_SWs:
            is_nfv_server_defined = False
            for h in range(0,self.k/2):
                self.net.add_edge(sw,i+h)
                self.net.add_edge(i+h,sw)
                self.net.node[i+h]['type']='host'
                self.all_hosts.append(i+h)

                # add one NFV server to every pod of servers
                if random.randint(0,1) is 1 and not is_nfv_server_defined:
                    self.net.node[i+h]['nfb'] = NetworkFunctionBox('nfv')
                    self.all_nfbs.append(i + h)
                    is_nfv_server_defined = True
            i+=self.k/2


    @set_ev_cls(event.EventSwitchEnter)
    def get_topology_data(self, ev):
        switch_list = get_switch(self.topology_api_app, None)
        switches = [switch.dp.id for switch in switch_list]
        self.net.add_nodes_from(switches)

        links_list = get_link(self.topology_api_app, None)

        links = [(link.src.dpid, link.dst.dpid, {'port': link.src.port_no}) for link in links_list]

        self.net.add_edges_from(links)
        '''
        print links_list
        print links
        print "\n\n\n\nGET TOPO DATA\n\n\n\n"
        print self.workThread.isAlive()
        '''


    def delete_flow(self, datapath):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        for dst in self.mac_to_port[datapath.id].keys():
            match = parser.OFPMatch(eth_dst=dst)
            mod = parser.OFPFlowMod(
                datapath, command=ofproto.OFPFC_DELETE,
                out_port=ofproto.OFPP_ANY, out_group=ofproto.OFPG_ANY,
                priority=1, match=match)
            datapath.send_msg(mod)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    #@set_ev_cls(stplib.EventPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg = ev.msg
        
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        dst = eth.dst
        src = eth.src

        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})

        self.logger.debug("packet in %s %s %s %s", dpid, src, dst, in_port)

        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = in_port

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

        actions = [parser.OFPActionOutput(out_port)]

        # install a flow to avoid packet_in next time
        if out_port != ofproto.OFPP_FLOOD:
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst)
            self.add_flow(datapath, 1, match, actions)

        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                  in_port=in_port, actions=actions, data=data)
        datapath.send_msg(out)

        #Assuming every flow that can reach here is a new flow
        if self.is_topology_discovered:
            if not self.is_greedy:
                S = [] # As in algorithm 1
                if len(self.policies) > 0:
                    print "Number of policies: ", len(self.policies)
                    p = self.policies.pop()
                else:
                    print "policy is empty"
                    os._exit(0)
                print "Ready to process policy", [nf.type for nf in p.chain]
                print "the policy has a length of ", p.len, "checking", len(p.chain)

                Q = self.find_all_nfbs_for_policy(p)

                d = {}
                prev = {}
                for v in Q:
                    d_i = [float('inf') for nf in p.chain]
                    d[v] = d_i
                    prev[v] = [] #undefined

                #fsrc = 0
                #fdst = 0

                if not Q:
                    print "Q is empty"
                    return

                fsrc = random.choice(Q)
                fdst = random.choice(Q)

                print "fsrc: ", fsrc
                print "fdst: ", fdst

                d[fsrc] = [0]

                while not not Q:
                    u_j = self.get_min_d(d,Q)
                    if u_j['u'] is fdst:
                        break
                    S.append(u_j['u'])
                    print "Path S is", S

                    #if current u is no longer needed, remove from Q
                    #if self.is_not_needed(u_j,p):
                    #print " "
                    #print "Q has a len of", len(Q), "and has ", Q, "and is about to remove:", u_j['u']
                    Q.remove(u_j['u'])
                    #print "after removing ", u_j['u'], "Q has a len of", len(Q), "and has ", Q

                    #B = self.find_all_nfbs_for_nf(p,u_j['j']+1)
                    #print "B is ", B
                    for v in Q:
                        #print "S", S
                        #print "Q", Q
                        #print "u", u_j['u']
                        #print "j", u_j['j']
                        #print "v", v
                        #print "p.len", p.len
                        #print "d", d
                        if u_j['j']+1 >= p.len:
                            return

                        if (p.chain[u_j['j']+1].type in self.net.node[v]['nfb'].typeset or 'NFV' in self.net.node[v][
                            'nfb'].typeset) \
                              and self.net.node[u_j['u']]['nfb'].cap > p.chain[u_j['j']+1].cap:
                            if d[v][u_j['j']+1] > (d[u_j['u']][u_j['j']] + 1/p.chain[u_j['j']].cap):
                                if v in self.get_path([u_j['u']],[u_j['j']],prev):
                                    d[v][u_j['j'] + 1] > d[u_j['u']][u_j['j']] + 1 / p.chain[u_j['j']].cap
            else:
                print "Greedy algorithm"
                if len(self.policies) > 0:
                    print "Number of policies: ", len(self.policies)
                    p = self.policies.pop()
                else:
                    print "policy is empty"
                    os._exit(0)

                B = self.find_all_nfbs_for_nf(p)
                B_bar = list(B)


                j = 0
                path = []

                fsrc = random.choice(self.all_hosts)

                path.append(fsrc)


                while j <= p.len:
                    if not B_bar:
                        j = j - 1
                        if j < 0:
                            path[:] = []
                            break
                        path.remove(path[len(path)-1]) # remove the last item
                        continue

                    u = self.get_min_v(path[len(path)-1], B, j)

                    B_bar[0][j].remove(u)

                    nk = p.chain[j]

                    if u not in path or self.has_cap(u, nk):
                        path.append(u)
                        self.net.node[u]['nfb'].cap = self.net.node[u]['nfb'].cap - nk.cap

                        if j==p.len:
                            break
                        else:
                            j = j + 1
                return path

                #os._exit(0)

    def has_cap(self, u, nk):
        if self.net.node[u]['nfb'].cap - nk.cap > 0:
            return True
        else:
            return False

    def get_min_v(self, end, B, j):

        for chain in B:
            all_d = [self.D[end][nfb_id] for nfb_id in chain[j]]
            #print all_d
            #print "min: ", min(all_d)
            for i in range(0,len(all_d)):
                #print "ith all_d: ", i, " : ",all_d[i]
                if all_d[i] == min(all_d):
                    #print "index: ", i, "nfb:  ", chain[j][i]
                    return chain[j][i]

    def get_path(self, dst, index, prev):
        path=[]
        u = dst
        j = index

        while (not prev) and j > 0:
            path.append(u)
            u = prev[u][j]
            j = j - 1
        path.append(u)

        return path

    def get_min_d(self, d, Q):
        min_val = d[Q[0]]
        u = Q[0]
        j = 0
        for q in Q:
            if min_val > min(d[q]):
                min_val = min(d[q])
                u = q
                j = d[q].index(min(d[q]))

        return {'u': u, 'j' : j, 'min': min_val}

    def is_not_needed(self, u_j, p):
        if u_j['j'] >= p.len-1:
            return True
        chains = p.reordered_chains
        chains.append(p.chain)

        for chain in chains:
            for i in range(u_j['j'], p.len):
                if (chain[i].type in self.net.node[u_j['u']]['nfb'].typeset or 'NFV' in self.net.node[u_j['u']]['nfb'].typeset)\
                  and self.net.node[u_j['u']]['nfb'].cap > chain[i].cap:
                    return False
        return True

    def find_all_nfbs_for_policy(self, p):
        Q = []
        chains = p.reordered_chains
        chains.append(p.chain)
        all_nfbs = list(self.all_nfbs)

        for chain in chains:
            for nf in chain:
                for nfb_id in all_nfbs:
                    #print "\n\n"
                    #print "nf.type ", nf.type
                    #print "self.net.node[nfb_id]['nfb'].typeset ",  self.net.node[nfb_id]['nfb'].typeset
                    #print "nfb_id", nfb_id
                    #print "nf.cap", nf.cap
                    #print "self.net.node[nfb_id]['nfb'].cap ",self.net.node[nfb_id]['nfb'].cap
                    if (nf.type in self.net.node[nfb_id]['nfb'].typeset or 'NFV' in self.net.node[nfb_id]['nfb'].typeset)\
                      and self.net.node[nfb_id]['nfb'].cap > nf.cap:
                        Q.append(nfb_id)
                        all_nfbs.remove(nfb_id)
                    #print "\n\n"
        return Q

    def find_all_nfbs_for_nf(self, p):
        B = []
        chains = list(p.reordered_chains)
        chains.append(p.chain)

        for i in range(0,len(chains)):
            B.append([])
            #suitable_nfbs = []
            for j in range(0, len(chains[i])):
                B[i].append([])
                for k in range(0,len(self.all_nfbs)):
                    if (chains[i][j].type in self.net.node[self.all_nfbs[k]]['nfb'].typeset or 'NFV' in self.net.node[self.all_nfbs[k]]['nfb'].typeset) \
                            and self.net.node[self.all_nfbs[k]]['nfb'].cap > chains[i][j].cap:
                        B[i][j].append(self.all_nfbs[k])
        return B


    '''
    @set_ev_cls(stplib.EventTopologyChange, MAIN_DISPATCHER)
    def _topology_change_handler(self, ev):
        dp = ev.dp
        dpid_str = dpid_lib.dpid_to_str(dp.id)
        msg = 'Receive topology change event. Flush MAC table.'
        self.logger.debug("[dpid=%s] %s", dpid_str, msg)

        if dp.id in self.mac_to_port:
            self.delete_flow(dp)
            del self.mac_to_port[dp.id]

    @set_ev_cls(stplib.EventPortStateChange, MAIN_DISPATCHER)
    def _port_state_change_handler(self, ev):
        dpid_str = dpid_lib.dpid_to_str(ev.dp.id)
        of_state = {stplib.PORT_STATE_DISABLE: 'DISABLE',
                    stplib.PORT_STATE_BLOCK: 'BLOCK',
                    stplib.PORT_STATE_LISTEN: 'LISTEN',
                    stplib.PORT_STATE_LEARN: 'LEARN',
                    stplib.PORT_STATE_FORWARD: 'FORWARD'}
        self.logger.info("[dpid=%s][port=%d] state=%s count=%d",
                          dpid_str, ev.port_no, of_state[ev.port_state], self.count)
        self.count += 1
        self.last_check = self.count
        print (time.time()-self.previous_time_stamp)
        self.previous_time_stamp = time.time()
        print self.workThread.isAlive()
    '''


