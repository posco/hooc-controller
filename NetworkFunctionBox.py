#from random import randint, choice, sample
import random

class NetworkFunctionBox:
    """An instance of network function box"""

    def __init__(self, type):
        self.default_typeset_list=['FW', 'IPS', 'RE', 'LB', 'IDS', 'Monitor']

        if type is 'hw':
            self.typeset = [random.choice(self.default_typeset_list)]
        elif type is 'sw':
            self.typeset = ['FW']
        elif type is 'nfv':
            #if the target NFB is not hardware, assign a random subset from the default list
            self.typeset = ['NFV']#random.sample(self.default_typeset_list,random.randint(1, 6))
        elif type is 'fw':
            self.typeset = ['FW']

        #assuming 1Gbps link
        #self.cap = random.randint(81274, 1488096) # in pps, packets per second
        self.cap = 1488096 # in pps, packets per second
