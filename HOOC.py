from Flow import Flow
from NetworkFunction import NetworkFunction
from NetworkFunctionBox import NetworkFunctionBox
from Policy import Policy

class HOOC:
    """implements hooc scheme and its algorithms"""

    def __init__(self):
        self.flows = [ ] # set of flows
        self.vms = [] # set of VMs

        # delay matrix
        self.D = matrix = [[0]*5 for i in range(len(self.vms))]

    def getDelay(self):

        # TODO: need transimission delay, via pingmesh
        # getTransmissionDelay
        # let's assume it's here and move to next

        